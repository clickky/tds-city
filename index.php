<?php
require 'vendor/autoload.php';

use GeoIp2\Database\Reader;

$active = true;
$data = [
    [
        'cities' => [],
        'name' => 'Back url',
        'link' => 'https://cpactions.com/api/v1.0/clk/track/proxy?disable_uu_validation=1&ad_id=1489485&site_id=9840&subsite_id={$source_id}&click_id={$click_id}'
    ],
    [
        'cities' => [
            'Moscow'
        ],
        'name' => 'Москва',
        'link' => 'https://app.appsflyer.com/ru.dublgis.dgismobile?af_prt=clickky&pid=clickky_int&c=Moskva&clickid={$clk_uid}&mac={$clk_mac}&advertising_id={$clk_google_aid}&af_siteid={$clk_source_id}'
    ],
    [
        'cities' => [
            'St Petersburg'
        ],
        'link' => 'https://app.appsflyer.com/ru.dublgis.dgismobile?af_prt=clickky&pid=clickky_int&c=Sanktpeterburg&clickid={$clk_uid}&mac={$clk_mac}&advertising_id={$clk_google_aid}&af_siteid={$clk_source_id}'
    ],
    [
        'cities' => [
            'Vinnytsia',
            'Lutsk',
            'Dnipro',
            'Kryvyi Rih',
            'Mariupol',
            'Zhytomyr',
            'Zaporizhia',
            'Ivano-Frankivsk',
            'Kiev',
            'Kropyvnytskyi',
            'Lviv',
            'Odesa',
            'Poltava',
            'Sumy',
            'Ternopil',
            'Kharkiv',
            'Kherson',
            'Khmelnytskyi',
            'Cherkasy',
            'Chernihiv',
            'Chernivtsi',
            'Mykolayiv',
            'Rivne',
            'Berdians"k',
            'Bila Tserkva',
            'Brovari',
            'Nikopol',
            'Pavlohrad',
            'Uzhhorod'
        ],
        'link' => 'https://app.adjust.com/cp5ml4?label=clickky&redirect_android=https://play.google.com/store/apps/details?id=com.ftband.mono&redirect_ios=https://itunes.apple.com/ru/app/id1287005205&adgroup={$clk_source_id}&install_callback=http://cpactions.com/api/v1.0/clk/track/pingback/event?event=install&uid={$clk_uid}'
    ]
];

header('Location: ' . check($data, $active), true, 303);
exit();

function check($data, $active)
{
    $ip = $_GET['clk_ip'];
    if ($ip === null) {
        $ip = get_ip_address();
    }

    $vars = array(
        '{$click_id}' => $_GET['click_id'],
        '{$source_id}' => $_GET['source_id'],
        '{$clk_google_aid}' => $_GET['clk_google_aid'],
        '{$clk_mac}' => $_GET['clk_mac'],
        '{$clk_uid}' => $_GET['clk_uid'],
        '{$clk_source_id}' => $_GET['clk_source_id'],
    );
    try {
        if (!$active) {
            throw new RuntimeException('empty');
        }

        $reader = new Reader(__DIR__ . '/GeoIP/GeoIP2-City.mmdb');
        $record = $reader->city($ip);
        $reader->close();

        $message = $ip;

        $item = false;
        foreach ($data as $value) {
            if (in_array($record->city->name, $value['cities'], true)) {
                $item = $value;
                break;
            }
        }
        $message .= ', clk_uid='.$_GET['clk_uid'];
        if ($item) {
            $message .= ', link='.strtr($item['link'], $vars);
            writeLog($message, $record->city->name);
            return strtr($item['link'], $vars);
        } else {
            throw new RuntimeException('empty');
        }
        throw new RuntimeException('empty');
    } catch (Exception $exception) {
        $message = $ip . ', ' . $exception->getMessage();
        if (isset($_GET['ip']) && $_GET['ip'] != $ip) {
            $message .= ', ' . $_GET['ip'];
        }
        $message .= ', clk_uid='.$_GET['clk_uid'];
        $message .= ', link='.strtr($data[0]['link'], $vars);
        writeLog($message, 0);
        return strtr($data[0]['link'], $vars);
    }
}

function writeLog($log, $url)
{
    file_put_contents(__DIR__ . '/logs/log_' . $url . '_' . date('j.n.Y') . '.txt', $log . PHP_EOL, FILE_APPEND);
}

/**
 * Retrieves the best guess of the client's actual IP address.
 * Takes into account numerous HTTP proxy headers due to variations
 * in how different ISPs handle IP addresses in headers between hops.
 */
function get_ip_address()
{
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
        return $_SERVER['HTTP_X_FORWARDED'];
    }
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_FORWARDED_FOR'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
        return $_SERVER['HTTP_FORWARDED'];
    }
    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 * @param $ip
 * @return bool
 */
function validate_ip($ip)
{
    if (strtolower($ip) === 'unknown') {
        return false;
    }
    // generate ipv4 network address
    $ip = ip2long($ip);
    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) {
            return false;
        }
        if ($ip >= 167772160 && $ip <= 184549375) {
            return false;
        }
        if ($ip >= 2130706432 && $ip <= 2147483647) {
            return false;
        }
        if ($ip >= 2851995648 && $ip <= 2852061183) {
            return false;
        }
        if ($ip >= 2886729728 && $ip <= 2887778303) {
            return false;
        }
        if ($ip >= 3221225984 && $ip <= 3221226239) {
            return false;
        }
        if ($ip >= 3232235520 && $ip <= 3232301055) {
            return false;
        }
        if ($ip >= 4294967040) {
            return false;
        }
    }
    return true;
}

